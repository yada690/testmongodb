package main

import (
	"fmt"
	"reflect"

	"github.com/davyxu/golog"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

var log = golog.New("test")

func init() {
	OpenMongoDB()
}

var session *mgo.Session

func main() {
	//test update commang to mongoDB
	//未完成 測試用interface{}的方式傳入
	// var col = session.DB("casino_accounts").C("bank")

	// var mapData map[string]interface{}
	// mapData = make(map[string]interface{})
	// mapData["bank_money"] = 100000
	// mapData["play_money"] = 2000
	// mapData["test"] = "12345"

	// for _, v := range mapData {
	// 	fmt.Println(v)
	// }

	// selector := bson.M{"uid": "94307c73-24c4-454f-bd3c-608e64d3d21d"}
	// //data := bson.M{"bank_money": 50000}//直接替換mongodb文檔的所有內容，其他的欄位也會不見
	// data := bson.M{"$set": bson.M{"bank_money": 90000}}
	// err := col.Update(selector, data)
	// if err != nil {
	// 	fmt.Println(err.Error())
	// } else {
	// 	fmt.Println("sucess!!")
	// }
	//==========

	//test select mongodb
	type SessionTable struct {
		Sessionid     string `bson:"sessionid"`
		Key           string `bson:"Key"`
		LastTimestamp string `bson:"lasttimestamp"`
	}

	data := SessionTable{}

	var col = session.DB("casino_admin").C("session")
	err := col.Find(bson.M{
		"sessionid": "",
	}).Select(bson.M{}).One(&data)
	if err == nil {
		fmt.Println(data)
	} else {
		fmt.Println(err.Error())
	}

	//test write errlog
	// var msg = &ErrMsg{
	// 	UID:             "94307c73-24c4-454f-bd3c-608e64d3d21d",
	// 	TYPE:            "sys",
	// 	CreateTimeStamp: 1518506700,
	// 	Data:            "test:0222-2 error",
	// }

	// err := WriteErrorLog(msg.UID, msg.TYPE, msg.CreateTimeStamp, msg.Data)
	// if err != nil {
	// 	fmt.Println(err.Error())
	// } else {
	// 	fmt.Println("sucess error")
	// }
	//==========

	//test write protocol log
	// var msgp = &ProtocolMsg{
	// 	UID:             "94307c73-24c4-454f-bd3c-608e64d3d21d",
	// 	TYPE:            "LoginACK",
	// 	Direction:       "out",
	// 	CreateTimeStamp: 1518506715,
	// 	Data: &LoginACK{
	// 		Result:      1,
	// 		Error:       "",
	// 		AccessToken: "test1234567890",
	// 	},
	// 	//Data:            "{\"test1\":\"testb\",\"test2\":456}",
	// }

	// err = WriteProtocolLog(msgp.UID, msgp.TYPE, msgp.Direction, msgp.CreateTimeStamp, msgp.Data)
	// if err != nil {
	// 	fmt.Println(err.Error())
	// } else {
	// 	fmt.Println("sucess protocol")
	// }
	//==========

	//test query logs
	//var logs PageErrLogs
	// var msg = &ProtocolMsg{
	// 	UID:             "94307c73-24c4-454f-bd3c-608e64d3d21d",
	// 	TYPE:            "LoginACK",
	// 	CreateTimeStamp: 1518506710,
	// 	Data: &LoginACK{
	// 		Result:      1,
	// 		Error:       "",
	// 		AccessToken: "test1234567890",
	// 	},
	// 	//Data:            "{\"test1\":\"testb\",\"test2\":456}",
	// }

	// fmt.Println(GetType(msg, true))
	// fmt.Println(GetType(msg.Data, true))
	// fmt.Println("test")
	//=========
}

func GetType(myvar interface{}, onlyname bool) string {
	if t := reflect.TypeOf(myvar); t.Kind() == reflect.Ptr {
		if onlyname {
			return t.Elem().Name()
		}
		return "*" + t.Elem().Name()

	} else {
		return t.Name()
	}
}

type LoginACK struct {
	Result int32 `sproto:"integer,0,name=Result"`

	Error string `sproto:"string,1,name=Error"`

	AccessToken string `sproto:"string,2,name=AccessToken"`
}

type ErrMsg struct {
	CreateTimeStamp int64  `bson:"createtimestamp"`
	UID             string `bson:"uid"`
	TYPE            string `bson:"type"`
	Data            string `bson:"data"`
}

type ProtocolMsg struct {
	CreateTimeStamp int64       `bson:"createtimestamp"`
	UID             string      `bson:"uid"`
	TYPE            string      `bson:"type"`
	Direction       string      `bson:"direction"`
	Data            interface{} `bson:"data"`
}

func WriteErrorLog(uid string, errType string, time int64, msg string) (err error) {
	err = session.Ping()
	if err != nil {
		return err
	}

	var col = session.DB("casino_logs").C("errors")
	var dbMsg *ErrMsg
	dbMsg = new(ErrMsg)
	dbMsg.UID = uid
	dbMsg.TYPE = errType
	dbMsg.CreateTimeStamp = time
	dbMsg.Data = msg

	err = col.Insert(&dbMsg)
	if err != nil {
		return err
	}

	return nil

}

func WriteProtocolLog(uid string, protocolType string, direct string, time int64, data interface{}) (err error) {
	err = session.Ping()
	if err != nil {
		return err
	}

	var msg = &ProtocolMsg{
		UID:             uid,
		TYPE:            protocolType,
		Direction:       direct,
		CreateTimeStamp: time,
		Data:            data,
		//Data:            "{\"test1\":\"testb\",\"test2\":456}",
	}

	serialData, err := bson.Marshal(msg)
	if err != nil {
		return err
	}

	var f interface{}
	err = bson.Unmarshal(serialData, &f)
	if err != nil {
		//TODO:错误处理
		return err
	}

	var col = session.DB("casino_logs").C("protocols")
	err = col.Insert(&f)
	if err != nil {
		return err
	}

	return nil
}

func OpenMongoDB() {
	// var settings = mongo.ConnectionURL{
	// 	Database: `casino_accounts`,
	// 	//Host:     `localhost:27017`,// local
	// 	Host: casino_util.MONGO_HOST, // dev
	// 	// User:     `demouser`,
	// 	// Password: `demop4ss`,
	// }

	var err error
	session, err = mgo.DialWithInfo(&mgo.DialInfo{
		//Addrs: "localhost:27017", // local
		Addrs: []string{"104.197.80.189:27017"}, // dev
	})
	// session, err = mongo.Open(settings)
	if err != nil {
		log.Errorln(fmt.Sprintf("MongoDB connect error: %q\n", err.Error()))
		panic(err.Error())
	}

	//defer session.Close()
}
