package util

import (
	"fmt"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//use
// logs := new(PageErrLogs)

// 	logs.col = session.DB("casino_logs").C("errors")
// 	logs.SetRowsPerPage(3)

// 	var qUID = "94307c73-24c4-454f-bd3c-608e64d3d21d"
// 	var qField = "createtimestamp"
// 	var qDesc = true
// 	var qPage = true

// 	err := logs.QueryFromUID(&qUID, &qField, &qDesc, &qPage)
// 	if err != nil {
// 		fmt.Println(err.Error())
// 	}

// 	fmt.Println(logs.currentMsgs)
// 	fmt.Println(logs.rowsPerPage)
// 	fmt.Println(logs.totalPage)
//==========

//page feature
type PageErrLogs struct {
	currentMsgs []ErrMsg
	currentPage int
	totalPage   int
	rowsPerPage int
	sortCond    string
	findCond    interface{}
	selectCond  interface{}
	col         *mgo.Collection
}

func (logs *PageErrLogs) QueryFromUID(uid *string, orderField *string, orderDesc *bool, usePage *bool) (err error) {
	err = session.Ping()
	if err != nil {
		return err
	}

	if logs.col == nil {
		return fmt.Errorf("未初始化 Struct 裡的Collection")
	}

	logs.findCond = bson.M{"uid": *uid}
	logs.selectCond = bson.M{}
	if *orderDesc { //降序
		logs.sortCond = "-" + *orderField
	} else { //升序
		logs.sortCond = *orderField
	}

	if !*usePage { //不使用page 全部傳回
		logs.currentPage = 1
		logs.totalPage = 1

		err = logs.col.Find(logs.findCond).Sort(logs.sortCond).Select(logs.selectCond).All(&logs.currentMsgs)
		if err != nil {
			return err
		}
		// var slice []ErrMsg
		// slice = logs.currentMsgs[:]

		logs.rowsPerPage = len(logs.currentMsgs)
	} else {
		//設定筆數
		count, err := logs.col.Find(logs.findCond).Count()
		if err != nil {
			return err
		}

		logs.currentPage = 1

		remind := count % logs.rowsPerPage
		if remind != 0 {
			logs.totalPage = count/logs.rowsPerPage + 1
		} else {
			logs.totalPage = count / logs.rowsPerPage
		}
		//依分頁取資料
		err = logs.col.Find(logs.findCond).Sort(logs.sortCond).Select(logs.selectCond).Skip(logs.currentPage - 1).Limit(logs.rowsPerPage).All(&logs.currentMsgs)
		if err != nil {
			return err
		}
	}

	return nil
}

func (logs *PageErrLogs) queryData(jsons string) error {
	if logs.col == nil {
		return fmt.Errorf("未初始化 Struct 裡的Collection")
	}

	return nil
}

func (logs *PageErrLogs) NextPage() error {

	return nil
}

func (logs *PageErrLogs) PreviosPage() error {

	return nil
}

func (logs *PageErrLogs) JumpPage(page *int32) error {
	if logs.rowsPerPage <= 0 {
		return fmt.Errorf("rowsPerPage 不可小於等於0")
	}
	//還要加東西

	return nil
}

func (logs *PageErrLogs) SetRowsPerPage(rows int) error {
	logs.rowsPerPage = rows
	return nil
}

//=========
